import { CanActivateFn } from '@angular/router';
import { ProjectService } from './project.service';
import { inject } from '@angular/core';

export const authGuard: CanActivateFn = (route, state) => {
  let service = inject(ProjectService);
  return service.getLoginStatus();
};
