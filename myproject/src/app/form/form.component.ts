import { Component } from '@angular/core';
import { ProjectService } from '../project.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent 
{
    formval :any
    qrCodeUrl: string;
    constructor(private service:ProjectService){
      this.formval={
        "fromstation":"",
        "tostation":"",
        "nooftickets":"",
        "ticketprice":"",
        "totalprice":""
      },
      this.qrCodeUrl=""
    }
    submit(){
      this.service.getQRCode(this.formval).subscribe(response => {
        const reader = new FileReader();
      reader.onloadend = () => {
        this.qrCodeUrl = reader.result as string;
      };
      reader.readAsDataURL(response);
      });
    }
    
}
