import { Component } from '@angular/core';
import { ProjectService } from '../project.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  user:any;
  constructor(public authService: ProjectService) {
    this.user = this.authService.username;
   }
   
  logout() {
    this.authService.setUserLogOut();
    console.log(this.authService.username);
  
  }
}
