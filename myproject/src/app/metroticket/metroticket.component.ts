import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../project.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-metroticket',
  templateUrl: './metroticket.component.html',
  styleUrls: ['./metroticket.component.css']
})
export class MetroticketComponent
{
  mticket :any;
  from_data:any;
  number_of_ticket:any;
  tprice:any;
  constructor(private service:ProjectService,private router:Router){}
  ngOnInit()
   {

     this.mticket=this.service.metroticket;
     this.number_of_ticket=this.service.ticketcount;
     this.tprice=this.number_of_ticket*this.mticket.fare;
  }
  book()
  {
    if(this.service.isUserLogged)
    {
      this.router.navigate(['payment']);
    }
    else
    {
      this.router.navigate(['login']);
    }
    
  }

  
}
