import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  loginStatus: Subject<any>;
  isUserLogged: boolean;
  metroticket: any;
  noofmetrotickets: any;
  username: any;
  private baseUrl = '/api';
  formData: any = {};
  selectedhotel: any;
  ticketcount:any;
  customer:any;
  roomprice:any;
  showDetails: any;
  number_of_people:any;
  selectedMovie : any;
  theatres: any;
  selectedTheatre: any;
  movieticket:any;
  mticket:any;
  noOfMovieTickets:any;
  
  constructor(private http: HttpClient) { 
    this.isUserLogged = false;
    this.loginStatus = new Subject();
    this.username = "";
  }

  custLogin(loginForm: any) {
    return this.http.get("custLogin/" + loginForm.emailId + "/" + loginForm.password).toPromise();
  }
  registerCustomer(cust: any): any {
    return this.http.post('registerCustomer', cust);
  }
  registerMetro(metro: any): any {
    return this.http.post('registerMetro',metro);
  }
  registerMetroTicket(ticket:any):any{
    return this.http.post('registerMetroTicket',ticket);
  }
  getAllMetros(): any{
    return this.http.get('getAllMetro');
  }
  registerHotelTicket(ticket:any):any{
    return this.http.post('registerHotel',ticket);
  }
  getQRCode(details: string): Observable<Blob> {
    return this.http.post(`${this.baseUrl}/generateQRCode`, details, { responseType: 'blob' });
  }
  getShowDetails(theatreId:any,showTime:any){
    return this.http.get("getmovieshows/"+theatreId+"/"+showTime);
  }
  getmetrodetails(ticketForm: any)
  {
    return this.http.get("getMetrodetails/" + ticketForm.fromStation + "/" + ticketForm.toStation).toPromise(); 
  }
  getAllTheatres(movieId:any){
    return this.http.get("/byMovieId/"+ movieId)
  }
  setFormData(data: any) {
    this.formData = data;
  }
  sendSms(phoneNumber: string): Observable<string> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const params = { phoneNumber };

    return this.http.get<string>(`/send-sms`, { params, headers });
  }
  sendOtp(email: string): Observable<string> {
    return this.http.get<string>(`/send-otp?email=${email}`);
  }
  gethoteldetails(){
    return this.http.get('GetAllHotels');
  }
  getAllMovies():any{
    return this.http.get('getAllMovies');
  }
  getFormData() {
    return this.formData;
  }
  updateavailableseats(showId: any, no_of_ticket: any): any {
    return this.http.post(`updateAvailableSeats/${showId}/${no_of_ticket}`, {});
  } 
   //Login
  setUserLogIn() {
    this.isUserLogged = true;

    //To Enable and Disable Login, Register and Logout
    this.loginStatus.next(true);
  }
  //Logout
  setUserLogOut() {
    this.isUserLogged = false;

    //To Enable and Disable Login, Register and Logout
    this.loginStatus.next(false);
  }
  //AuthGuard
  getLoginStatus(): boolean {
    return this.isUserLogged;
  }

  //To Enable and Disable Login, Register and Logout
  getStatusLogin(): any {
    return this.loginStatus.asObservable();
  }

  
}
