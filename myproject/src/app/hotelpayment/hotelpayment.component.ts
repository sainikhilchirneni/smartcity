import { Component } from '@angular/core';
import { ProjectService } from '../project.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hotelpayment',
  templateUrl: './hotelpayment.component.html',
  styleUrls: ['./hotelpayment.component.css']
})
export class HotelpaymentComponent 
{
  ticket:any;
  pticket:any;
  tcount:any;
  totalprice:any;
  hticket:any;
  no_of_persons:any;
  roomprice: any;
  constructor(private service:ProjectService,private router: Router){
    this.hticket=this.service.selectedhotel;
    this.roomprice=this.service.roomprice;
    this.hticket=this.service.number_of_people;
  }
  ngOnInit() {
  }
  pay()
  {
   this.ticket=
    {
      "numOfpeople":this.hticket,
      "totalPrice":this.totalprice,
      "roomprice":this.roomprice,
      "hotel":{
        "hotelId":this.hticket.hotelId
      },
      "customer":{
        "custId":this.service.customer.custId
      }
    }
    this.service.registerHotelTicket(this.ticket).subscribe((data:any) =>{

    });
    this.router.navigate(['metro']);
  }

}
