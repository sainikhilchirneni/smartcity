import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../project.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hotelslist',
  templateUrl: './hotelslist.component.html',
  styleUrls: ['./hotelslist.component.css']
})
export class HotelslistComponent  implements OnInit{

  hotels: any;
  images = [
    { url: 'https://cache.marriott.com/content/dam/marriott-renditions/HYDMD/hydmd-facade-9330-hor-clsc.jpg?output-quality=70&interpolation=progressive-bilinear&downsize=2880px:*' },
    { url: 'https://www.itchotels.com/content/dam/itchotels/in/umbrella/itc/hotels/itckohenur-hyderabad/images/overview/headmast-desktop/hotel-facade.png', delay: '2s' },
    { url: 'https://imkarchitects.com/images/projects/business-hotel/taj-krishna/2.jpg', delay: '4s' },
    { url: 'https://www.ahstatic.com/photos/6687_ho_00_p_2048x1536.jpg', delay: '6s' },
    { url: 'https://s7d1.scene7.com/is/image/marriotts7prod/hydsi-exterior-1144:Classic-Hor?wid=2880&fit=constrain', delay: '8s' },
    
  ];
  ngOnInit()
  {
    this.service.gethoteldetails().subscribe((data:any) => {
      this.hotels = data;
      console.log(this.hotels);
    });
  }
  constructor(private service: ProjectService, private router: Router) {
  }

  booknow(hotel:any)
  {
    this.service.selectedhotel=hotel;
    this.router.navigate(['hoteldesc']);
  }
}
