import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { SignupComponent } from './signup/signup.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MoviesComponent } from './movies/movies.component';
import { MetroComponent } from './metro/metro.component';
import { MovieticketComponent } from './movieticket/movieticket.component';
import { MetroticketComponent } from './metroticket/metroticket.component';
import { MetroregisterComponent } from './metroregister/metroregister.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { RestuarantsComponent } from './restuarants/restuarants.component';
import { MyhomeComponent } from './myhome/myhome.component';
import { HotelslistComponent } from './hotelslist/hotelslist.component';
import { HoteldescComponent } from './hoteldesc/hoteldesc.component';
import { FormComponent } from './form/form.component';
import { HotelreceiptComponent } from './hotelreceipt/hotelreceipt.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ToastrModule } from 'ngx-toastr';
import { MybookingsComponent } from './mybookings/mybookings.component';
import { PaymentComponent } from './payment/payment.component';
import { HotelpaymentComponent } from './hotelpayment/hotelpayment.component';
import { MoviedesComponent } from './moviedes/moviedes.component';
import { MovipaymentComponent } from './movipayment/movipayment.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    SignupComponent,
    MoviesComponent,
    MetroComponent,
    MovieticketComponent,
    MetroticketComponent,
    MetroregisterComponent,
    ForgetpasswordComponent,
    RestuarantsComponent,
    MyhomeComponent,
    HotelslistComponent,
    HoteldescComponent,
    FormComponent,
    HotelreceiptComponent,
    MybookingsComponent,
    PaymentComponent,
    HotelpaymentComponent,
    MoviedesComponent,
    MovipaymentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    ModalModule.forRoot(),
    CarouselModule.forRoot(),
    ToastrModule.forRoot({
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
