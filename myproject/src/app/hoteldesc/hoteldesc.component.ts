import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../project.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hoteldesc',
  templateUrl: './hoteldesc.component.html',
  styleUrls: ['./hoteldesc.component.css']
})
export class HoteldescComponent  implements OnInit {

  formData: any = {}; // Object to store form data
  to_date: any;
  from_date: any;
  num_persons: any;
  num_days: any;
  num_rooms:any;
  Hotel:any;
  images = [
    { url: 'https://cache.marriott.com/content/dam/marriott-renditions/HYDSI/hydsi-club-9340-hor-wide.jpg?output-quality=70&interpolation=progressive-bilinear&downsize=1336px:*' },
    { url: 'https://www.itchotels.com/content/dam/itchotels/in/umbrella/itc/hotels/itckohenur-hyderabad/images/overview/headmast-desktop/towers.png', delay: '2s' },
    { url: 'https://www.tridenthotels.com/-/media/trident-hotel/trident-hyderabad/room--suites/room-detail/executive-suite/banner1920x1080.jpg', delay: '4s' },
    { url: 'https://www.ahstatic.com/photos/6687_rokge_00_p_2048x1536.jpg', delay: '6s' },
    { url: 'https://cache.marriott.com/content/dam/marriott-renditions/HYDCY/hydcy-guestroom-0025-hor-wide.jpg?output-quality=70&interpolation=progressive-bilinear&downsize=1336px:*', delay: '8s' },
    
  ];

  ngOnInit()
  {
    this.Hotel=this.service.selectedhotel;
  }
  constructor(private service: ProjectService, private router: Router)
  {

  }
  book()
  {
    if (!this.service.isUserLogged) {
      this.router.navigate(['login']); // Replace 'login' with the actual route for your login page.
    } else {
      this.formData.fromDate = this.from_date;
    this.formData.toDate = this.to_date;
    this.formData.numPersons = this.num_persons;
    this.formData.numDays = this.num_days;
    this.service.setFormData(this.formData);
      this.router.navigate(['hotelreceipt']);
    }
    
    //this.router.navigate(['hotelreciept']);
  }
}
