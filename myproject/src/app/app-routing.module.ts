import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { MoviesComponent } from './movies/movies.component';
import { MetroComponent } from './metro/metro.component';
import { MetroticketComponent } from './metroticket/metroticket.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { RestuarantsComponent } from './restuarants/restuarants.component';
import { MyhomeComponent } from './myhome/myhome.component';
import { HotelslistComponent } from './hotelslist/hotelslist.component';
import { HoteldescComponent } from './hoteldesc/hoteldesc.component';
import { HotelreceiptComponent } from './hotelreceipt/hotelreceipt.component';
import { MoviedesComponent } from './moviedes/moviedes.component';
import { MovieticketComponent } from './movieticket/movieticket.component';
import { PaymentComponent } from './payment/payment.component';
import { MovipaymentComponent } from './movipayment/movipayment.component';
import { HotelpaymentComponent } from './hotelpayment/hotelpayment.component';



const routes: Routes = [
  {path:"login", component:LoginComponent},
  {path:"signup", component:SignupComponent},
  {path:"login/signup", component:LoginComponent},
  {path:"signup/login", component:SignupComponent},
  {path:"home", component:HomeComponent},
  {path:"movies", component:MoviesComponent},
  {path:"metro", component:MetroComponent},
  {path:"metroticket",component:MetroticketComponent},
  {path:"forgetpassword", component:ForgetpasswordComponent},
  {path:"login/forgetpassword", component:LoginComponent},
  {path:"restuarants", component:RestuarantsComponent},
  {path:"myhome",component:MyhomeComponent},
  {path:"hotellist",component:HotelslistComponent},
  {path:"hoteldesc",component:HoteldescComponent},
  {path:"hotelreceipt",component:HotelreceiptComponent},
  {path:"moviedes",component:MoviedesComponent},
  {path:"movieticket",component:MovieticketComponent},
  {path:"payment",component:PaymentComponent},
  {path:"moviepayment",component:MovipaymentComponent},
  {path:"hotelpayment",component:HotelpaymentComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
