import { Component, OnInit} from '@angular/core';
import { ProjectService } from '../project.service';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit{

  email: string = '';
  otp: string = '';
 

  constructor(private authService: ProjectService) { }

  sendOTP() {
    // Assuming AuthService has a method to send OTP to the email
    this.authService.sendOtp(this.email).subscribe(
      (data: any) => {
        console.log(data);
      }
    );
  }
  

  verifyOTP() {
    // // Assuming AuthService has a method to verify the entered OTP
    // this.authService.verifyOTP(this.email, this.otp).subscribe(
    //   response => {
    //     console.log('OTP verified successfully');
    //     // Add logic to navigate to a new password setup page or perform other actions
    //   },
    //   error => {
    //     console.error('Error verifying OTP', error);
    //     // Handle error, show a message, etc.
    //   }
    // );
  }
  ngOnInit(){
    
  }

} 


