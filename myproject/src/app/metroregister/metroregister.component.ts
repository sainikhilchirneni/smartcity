import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../project.service';

@Component({
  selector: 'app-metroregister',
  templateUrl: './metroregister.component.html',
  styleUrls: ['./metroregister.component.css']
})
export class MetroregisterComponent implements OnInit{

  fromStation: string = '';
  toStation: string = '';
  fare: number = 0;
  duration: number = 0;
  distance: number = 0;

  constructor(private projectService: ProjectService) {}
  ngOnInit(){
   
  }

  registerMetro(){
    const metroData = {
      fromStation: this.fromStation,
      toStation: this.toStation,
      fare: this.fare,
      duration: this.duration,
      distance: this.distance
    };

    this.projectService.registerMetro(metroData).subscribe((data : any) => {
      console.log('Metro registered successfully!', data);
      // You can handle the response or perform any other actions here
    });
  }

}
