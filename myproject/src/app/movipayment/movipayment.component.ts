import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../project.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movipayment',
  templateUrl: './movipayment.component.html',
  styleUrls: ['./movipayment.component.css']
})
export class MovipaymentComponent implements OnInit  {
  numtickets:any;
  showId:any;
  constructor(private service: ProjectService,private router:Router){}
  ngOnInit()  {
    this.numtickets=this.service.noOfMovieTickets;
    this.showId = this.service.showDetails[0].showId;
    
  }
  pay(){

    this.service.updateavailableseats(this.showId,this.numtickets).subscribe((data:any) => {
      console.log(data);
    });
    this.router.navigate(['movies']);
  }

}
