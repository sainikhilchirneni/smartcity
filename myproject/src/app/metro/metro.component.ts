import { Component, OnInit, } from '@angular/core';
import { ProjectService } from '../project.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-metro',
  templateUrl: './metro.component.html',
  styleUrls: ['./metro.component.css']
})
export class MetroComponent implements OnInit  {
  fromStation: any;
  toStation: any;
  noOfTickets: any;
  metros: any;
  ticket: any;
  finalticket: any;

  constructor(private service: ProjectService, private router: Router) {
    this.finalticket={
      "ticket":this.ticket,
      "nooftickets":this.noOfTickets
    }
  }

  ngOnInit() {
    this.service.getAllMetros().subscribe((data: any) => {
      this.metros = data;
    });
  }

  async bookTicket(ticketForm: any) {

    await this.service.getmetrodetails(ticketForm).then((custData: any) => {
      this.ticket = custData;
      this.service.metroticket = this.ticket;
     this.service.ticketcount=this.noOfTickets;
    });
    if (this.ticket != null) {
      this.router.navigate(['metroticket']);
    } else {
      alert("Invalid Credentials");
    }
  

    
  }
}