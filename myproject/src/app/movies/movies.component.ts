import { Component } from '@angular/core';
import { ProjectService } from '../project.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent {

  movies: any;

  constructor(private service:ProjectService, private router: Router){

  }

  images = [
    { url: 'https://i.ytimg.com/vi/7Y5q41D8_hs/maxresdefault.jpg' },
    { url: 'https://stat4.bollywoodhungama.in/wp-content/uploads/2023/07/Salaar-322x161.jpg', delay: '2s' },
    { url: 'https://wallpapercave.com/wp/wp12694295.jpg', delay: '4s' },
    { url: 'https://i.ytimg.com/vi/ZgVs2S5jnzA/sddefault.jpg?v=64b4e81f', delay: '6s' },
    { url: 'https://preview.redd.it/discover-the-best-of-tollywood-52-must-see-telugu-movies-v0-aaxtby7yeura1.png?width=1886&format=png&auto=webp&s=746f51974d41c5ddea6da9db864dbb2c8d2536f7', delay: '8s' },
    // Add more images with corresponding delay values if needed
  ];
  ngOnInit() {
    this.service.getAllMovies().subscribe((data:any) => {
      this.movies =data;
    });
      
    }
  async selectMovie(movie: any){
    this.service.selectedMovie = movie;
    await this.service.getAllTheatres(movie.movieId).subscribe((data:any) =>{
      this.service.theatres = data;
    });
    this.router.navigate(['moviedes']);
  }
  
}

