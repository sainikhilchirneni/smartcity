import { Component, OnInit, TemplateRef } from '@angular/core';
import { ProjectService } from '../project.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hotelreceipt',
  templateUrl: './hotelreceipt.component.html',
  styleUrls: ['./hotelreceipt.component.css']
})
export class HotelreceiptComponent implements OnInit {
  modalRef: BsModalRef | undefined;
  formData: any;
  hoteldata:any;
  totalprice:any;
  name:any;
  constructor(private service: ProjectService,private router: Router) {}

  ngOnInit(){
    this.hoteldata=this.service.selectedhotel;
    this.formData = this.service.getFormData();
    this.totalprice=this.hoteldata.roomprice*this.formData.numDays;
    this.name=this.service.username;
    this.service.roomprice=this.hoteldata.roomprice;
    
  }
  book()
  {
    this.router.navigate(['hotelpayment']);
  }
}
