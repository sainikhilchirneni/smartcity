import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../project.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit{

  customer: any;
  constructor(private service: ProjectService, private router:Router){
    this.customer = {
      "custName":"",
      "custPhone":"",
      "custEmail":"",
      "custPassword":""
    }

  }
  ngOnInit(){
    
  }

  isValidPhoneNumber(phoneNumber: string): boolean {
    // Phone number must start with 9, 8, 7, or 6 and contain exactly 10 digits
    const phoneRegex = /^[6-9]\d{9}$/;
    return phoneRegex.test(phoneNumber);
  }
  isValidEmail(email: string): boolean {
    // Email must contain "@" and end with ".com"
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }
  isValidPassword(password: string): boolean {
    // Password must have at least 8 characters, one uppercase, one lowercase, one special character, and one digit
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    return passwordRegex.test(password);
  }

  isValidLength(password: string): boolean {
    // Password must have at least 8 characters
    return password.length >= 8;
  }
  register(){
    this.service.registerCustomer(this.customer).subscribe((data: any) => {
    });
    this.service.sendOtp(this.customer.custEmail).subscribe((data:any) => {
    });
    this.service.sendSms(this.customer.custPhone).subscribe((data:any) => {
    });
    this.router.navigate(['login']);
  }



}
