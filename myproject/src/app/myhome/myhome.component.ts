import { Component } from '@angular/core';

@Component({
  selector: 'app-myhome',
  templateUrl: './myhome.component.html',
  styleUrls: ['./myhome.component.css']
})
export class MyhomeComponent 
{
  images = [
    { url: 'https://cache.marriott.com/content/dam/marriott-renditions/HYDMD/hydmd-facade-9330-hor-clsc.jpg?output-quality=70&interpolation=progressive-bilinear&downsize=2880px:*' },
    { url: 'https://static.toiimg.com/thumb/msid-89515763,width-1280,height-720,resizemode-4/89515763.jpg', delay: '2s' },
    { url: 'https://preview.redd.it/discover-the-best-of-tollywood-52-must-see-telugu-movies-v0-aaxtby7yeura1.png?width=1886&format=png&auto=webp&s=746f51974d41c5ddea6da9db864dbb2c8d2536f7', delay: '4s' },
    { url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Hyderabad_Metro.jpg/1200px-Hyderabad_Metro.jpg', delay: '6s' },
    // { url: 'https://preview.redd.it/discover-the-best-of-tollywood-52-must-see-telugu-movies-v0-aaxtby7yeura1.png?width=1886&format=png&auto=webp&s=746f51974d41c5ddea6da9db864dbb2c8d2536f7', delay: '10s' },
    // Add more images with corresponding delay values if needed
  ];

}
