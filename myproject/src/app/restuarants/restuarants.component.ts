import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-restuarants',
  templateUrl: './restuarants.component.html',
  styleUrls: ['./restuarants.component.css']
})
export class RestuarantsComponent implements OnInit {

  slides = [
    { image: 'https://images.unsplash.com/photo-1621909321963-2276c9660298?auto=format&fit=crop&q=80&w=1834&h=800&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D', active: true },
    { image: 'https://images.unsplash.com/photo-1576906131787-4921116d8195?auto=format&fit=crop&q=80&w=1932&h=800&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1pYWdlfHx8fGVufDB8fHx8fA%3D%3D', active: false },
    { image: 'https://images.unsplash.com/photo-1558512924-322955174c44?auto=format&fit=crop&q=80&w=1974&h=800&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D', active: false }
  ];

  constructor() { }

  ngOnInit() {
    // Automatically slide the carousel every 3 seconds (3000 milliseconds)
    setInterval(() => {
      this.nextSlide();
    }, 3000);
  }

  nextSlide() {
    const activeSlideIndex = this.slides.findIndex(slide => slide.active);
    if (activeSlideIndex !== -1) {
      this.slides[activeSlideIndex].active = false;
    }

    const nextSlideIndex = (activeSlideIndex + 1) % this.slides.length;
    this.slides[nextSlideIndex].active = true;
  }

}
