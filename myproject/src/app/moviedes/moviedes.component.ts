import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../project.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-moviedes',
  templateUrl: './moviedes.component.html',
  styleUrls: ['./moviedes.component.css']
})
export class MoviedesComponent implements OnInit {

  movie: any;
  theatres : any;

  constructor(private service:ProjectService,private router: Router){}

  ngOnInit(){

    this.movie = this.service.selectedMovie;
    this.theatres = this.service.theatres;

  }
  show1(theatre:any){
      
    this.service.getShowDetails(theatre.theatreId,"11:00 am").subscribe((data:any) => {
      this.service.showDetails = data;
      this.service.selectedTheatre = theatre;
      this.router.navigate(['movieticket']);
    });

  }

  show2(theatre:any){
    this.service.getShowDetails(theatre.theatreId,"02:00 pm").subscribe((data:any) => {
      this.service.showDetails = data;
      this.service.selectedTheatre = theatre;
      this.router.navigate(['movieticket']);
    });
  }

  show3(theatre:any){
    this.service.getShowDetails(theatre.theatreId,"06:00 pm").subscribe((data:any) => {
      this.service.showDetails = data;
      this.service.selectedTheatre = theatre;
      this.router.navigate(['movieticket']);
    });
  }

  show4(theatre:any){
    this.service.getShowDetails(theatre.theatreId,"11:00 pm").subscribe((data:any) => {
      this.service.showDetails = data;
      this.service.selectedTheatre = theatre;
      this.router.navigate(['movieticket']);
    });
  }

}
