import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../project.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  loginId: any;
  password: any;
  customers: any;
  cust: any;

  //Date Of Join: MM/dd/yyyy

  constructor(private service: ProjectService, private router:Router,private toastr:ToastrService) {
    
  }

  ngOnInit() {
  }
  

  async loginSubmit(loginForm: any) {
    await this.service.custLogin(loginForm).then((custData: any) => {
        this.cust = custData;
      });

      if (this.cust != null) {
        this.service.username = this.cust.custName;
        this.service.setUserLogIn();
        this.toastr.success("Login SuccessFul");
        this.router.navigate(['myhome']);
      } else {
        this.toastr.error("Login Failed");
      }
    }

}






