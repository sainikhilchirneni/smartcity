import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectService } from '../project.service';

@Component({
  selector: 'app-movieticket',
  templateUrl: './movieticket.component.html',
  styleUrls: ['./movieticket.component.css']
})
export class MovieticketComponent implements OnInit {
  movie:any;
  theatre: any;
  show: any;
  numberofmovietickets:any;

  constructor(private router:Router,private service:ProjectService){}
  ngOnInit() {
  this.movie=this.service.selectedMovie;
  this.theatre=this.service.selectedTheatre;
  this.show=this.service.showDetails;
  }
  book()
  {
    if(this.service.isUserLogged)
    {
      this.service.noOfMovieTickets=this.numberofmovietickets;
    this.router.navigate(['moviepayment']);
    }
    else
    {
      this.router.navigate(['login']);
    }
  }

}
