import { Component } from '@angular/core';
import { ProjectService } from '../project.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent
 {
  ticket:any;
  pticket:any;
  tcount:any;
  totalprice:any;
  constructor(private service:ProjectService,private router: Router){
    this.pticket=this.service.metroticket;
    this.tcount=this.service.ticketcount;
    this.totalprice=this.tcount*this.pticket.fare;
  }
  ngOnInit() {
  }
  pay()
  {
   this.ticket=
    {
      "numOfTickets":this.tcount,
      "totalPrice":this.totalprice,
      "metro":{
        "metroId":this.pticket.metroId
      },
      "customer":{
        "custId":this.service.customer.custId
      }
    }
    this.service.registerMetroTicket(this.ticket).subscribe((data:any) =>{

    });
    this.router.navigate(['metro']);
  }

}
