package com.dao;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class MobileOtpService {
	
	private static final String ACCOUNT_SID = "AC1e96c21b2e8e2b68defb43a716f36afa";
    private static final String AUTH_TOKEN = "b3750c488ea9c7c8a4f65eaf04e4c9bd";
    private static final String TWILIO_PHONE_NUMBER = "+12563644184";
    
    public String generateOtp() {
        // Generate a random 6-digit OTP
        Random random = new Random();
        int otp = 100000 + random.nextInt(900000);
        return String.valueOf(otp);
    }

    public void sendSms(String toPhoneNumber, String messageBody) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(
                new PhoneNumber(toPhoneNumber),  // Recipient's phone number
                new PhoneNumber(TWILIO_PHONE_NUMBER),  // Your Twilio phone number
                messageBody)
                .create();

        System.out.println("SMS sent with SID: " + message.getSid());
    }

}
