package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.MetroTicket;

@Service
public class MetroTicketDAO {
	
	@Autowired
	MetroTicketRepository metroTicketRepo;
	
	
	public MetroTicket saveTicket(MetroTicket metroTicket){
		return metroTicketRepo.save(metroTicket);
	}
	

}
