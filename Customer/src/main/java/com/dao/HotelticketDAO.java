package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Hoteltickets;
@Service
public class HotelticketDAO {

	@Autowired
	HotelticketRepository hotelticketRepo;
	public Hoteltickets RegisterHotelticket(Hoteltickets hotelticket)
	{
		return hotelticketRepo.save(hotelticket);
	}
	public Hoteltickets UpdateHotelticket(Hoteltickets hotelticket)
	{
		return hotelticketRepo.save(hotelticket);
	}
	public Hoteltickets getHotelticketbyId(int hotelticketId)
	{
		return hotelticketRepo.findById(hotelticketId).orElse(null);
	}
	public List<Hoteltickets> getAllHoteltickets()
	{
		return hotelticketRepo.findAll();
	}
}
