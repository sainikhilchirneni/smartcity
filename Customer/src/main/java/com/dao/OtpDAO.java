package com.dao;

import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.model.Customer;

@Service
public class OtpDAO {
	
	@Autowired
    private JavaMailSender javaMailSender;

	@Autowired
	private CustomerRepository custRepo;

    public String generateOtp() {
        // Generate a random 6-digit OTP
        Random random = new Random();
        int otp = 100000 + random.nextInt(900000);
        return String.valueOf(otp);
    }

    public void sendOtpByEmail(String email) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(email);
        message.setSubject("SMART CITY ");
        message.setText("Thank You for Registering to Smartcity");

        javaMailSender.send(message);
    }
    
    public boolean isEmailExists(String email) {
        // Check if the email exists in the database
        Optional<Customer> customer = custRepo.findBycustEmail(email);
        return customer.isPresent();
    }

}
