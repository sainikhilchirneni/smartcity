package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Customer;

@Service
public class CustomerDAO 
{
	
	@Autowired
	CustomerRepository custRepo;
	public Customer custLogin(String emailId, String password) {
		return custRepo.custLogin(emailId, password);
	}
	public Customer registerCust(Customer cust) {
		return custRepo.save(cust);
	}
	public List<Customer> getAllCustomers() {
		return custRepo.findAll();
	}

	
}
