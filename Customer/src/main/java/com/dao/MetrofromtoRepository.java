package com.dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.model.MetroFromTo;
@Repository
public interface MetrofromtoRepository extends JpaRepository<MetroFromTo,Integer> {

}
