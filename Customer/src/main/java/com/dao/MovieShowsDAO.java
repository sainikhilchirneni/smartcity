package com.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.MovieShows;


@Service
public class MovieShowsDAO {

	@Autowired
	MovieShowsRepository movieShowsRepo;
	
	public MovieShows registerMovieShows(MovieShows movieshows) {
		return movieShowsRepo.save(movieshows);
	}
	public MovieShows getShows(int theatreId, String showTime){
		return movieShowsRepo.findByTheatre_TheatreIdAndShowTime(theatreId, showTime);
	}
	@Transactional
    public MovieShows updateAvailableSeats(int showId, int newAvailableSeats) {
        MovieShows movieShows = movieShowsRepo.findById(showId).orElse(null);

        if (movieShows != null) {
            movieShows.setAvailableSeats(newAvailableSeats);
            movieShowsRepo.save(movieShows);
        }

        return movieShows;
    }
}
