package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Theatre;

@Service
public class TheatreDAO {

	@Autowired
	TheatreRepository theatreRepo;
	
	public Theatre registerTheatre(Theatre theatre) {
		return theatreRepo.save(theatre);
	}
	
	public List<Theatre> getTheatres(int movieId){
		return theatreRepo.findByMovies_MovieId(movieId);
	}
}
