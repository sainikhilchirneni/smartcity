package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.MetroFromTo;

@Service
public class MetroFromToDAO 
{
	@Autowired
	MetrofromtoRepository MetroFromtoRepo;
    public MetroFromTo RegisterMetroFromto(MetroFromTo metrofromto)
    {
    	return MetroFromtoRepo.save(metrofromto);
    }
    public List<MetroFromTo> getAllMetroFromto()
    {
    	return MetroFromtoRepo.findAll();
    }
}
