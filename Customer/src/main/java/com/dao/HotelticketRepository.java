package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.model.Hoteltickets;
@Repository
public interface HotelticketRepository extends JpaRepository<Hoteltickets,Integer> {

}
