package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.model.Theatre;

@Repository
public interface TheatreRepository extends JpaRepository<Theatre,Integer>  {
	
	List<Theatre> findByMovies_MovieId(int movieId);

}
