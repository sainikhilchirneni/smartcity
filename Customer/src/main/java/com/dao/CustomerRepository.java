package com.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Integer> {
	
	@Query("from Customer where custEmail = :emailId and custPassword = :password")
	Customer custLogin(@Param("emailId") String emailId, @Param("password") String password);

	Optional<Customer> findBycustEmail(String email);

}
