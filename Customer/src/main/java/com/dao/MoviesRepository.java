package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.model.Movies;

@Repository
public interface MoviesRepository extends JpaRepository<Movies,Integer>  {

}
