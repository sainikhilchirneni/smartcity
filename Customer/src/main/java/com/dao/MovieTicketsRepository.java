package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.model.MovieTickets;

@Repository
public interface MovieTicketsRepository extends JpaRepository<MovieTickets,Integer>  {

}
