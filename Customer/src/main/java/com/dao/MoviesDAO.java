package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Movies;

@Service
public class MoviesDAO {

	@Autowired
	MoviesRepository moviesRepo;
	
	public Movies registerMovies(Movies movie) {
		return moviesRepo.save(movie);
	}
	
	public List<Movies> getAllMovies(){
		return moviesRepo.findAll();
	}
}
