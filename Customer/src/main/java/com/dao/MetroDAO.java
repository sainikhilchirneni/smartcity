package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.model.Metro;

@Service
public class MetroDAO {
	
	@Autowired
	MetroRepository metroRepo;
	
	public Metro getMetroDetails(String fromStation, String toStation) {
		return metroRepo.getMetroDetails(fromStation, toStation);
	}
	
	public Metro metroRegister(Metro metro){
		return metroRepo.save(metro);
	}
	public List<Metro> getAllMetro(){
		return metroRepo.findAll();
	}
	public Metro findMetroById(int metroId){
		return metroRepo.findById(metroId).orElse(null);
	}
}
