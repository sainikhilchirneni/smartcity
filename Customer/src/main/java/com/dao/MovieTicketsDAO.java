package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.MovieTickets;

@Service
public class MovieTicketsDAO {

	@Autowired
	MovieTicketsRepository movieTicketsRepo;
	
	public MovieTickets registerMovieTickets(MovieTickets movietickets) {
		return movieTicketsRepo.save(movietickets);
	}
}
