package com.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Hotel;
@Service
public class HotelDAO {

	@Autowired
	HotelRepository hotelRepo;
	public Hotel RegisterHotel(Hotel hotel)
	{
		return hotelRepo.save(hotel);
	}
	public Hotel UpdateHotel(Hotel hotel)
	{
		return hotelRepo.save(hotel);
	}
	public Hotel getHotelbyId(int hotelId)
	{
		return hotelRepo.findById(hotelId).orElse(null);
	}
	public List<Hotel> getAllHotels()
	{
		return hotelRepo.findAll();
	}
}
