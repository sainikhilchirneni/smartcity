package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Metro;

@Repository
public interface MetroRepository extends JpaRepository<Metro, Integer>{
	
	@Query("from Metro where fromStation = :fromStation and toStation = :toStation")
	Metro getMetroDetails(@Param("fromStation") String fromStation, @Param("toStation") String toStation);

}
