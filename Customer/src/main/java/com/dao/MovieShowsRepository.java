package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.model.MovieShows;

@Repository
public interface MovieShowsRepository extends JpaRepository<MovieShows,Integer> {

	MovieShows findByTheatre_TheatreIdAndShowTime(int theatreId, String showTime);
}
