package com.model;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Theatre {
	
	@Id @GeneratedValue
	private int theatreId;
	private String theatreName;
	private String theatreAddress;
	
	@ManyToOne
	@JsonIgnore
	private Movies movies;
	
	public Movies getMovies() {
		return movies;
	}
	public void setMovies(Movies movies) {
		this.movies = movies;
	}
	@OneToMany(mappedBy="theatre")
	@JsonIgnore
	List<MovieShows> movieshows=new ArrayList<>();
	
	public Theatre() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Theatre(int theatreId, String theatreName, String theatreAddress) {
		super();
		this.theatreId = theatreId;
		this.theatreName = theatreName;
		this.theatreAddress = theatreAddress;
	}
	public int getTheatreId() {
		return theatreId;
	}
	public void setTheatreId(int theatreId) {
		this.theatreId = theatreId;
	}
	public String getTheatreName() {
		return theatreName;
	}
	public void setTheatreName(String theatreName) {
		this.theatreName = theatreName;
	}
	public String getTheatreAddress() {
		return theatreAddress;
	}
	public void setTheatreAddress(String theatreAddress) {
		this.theatreAddress = theatreAddress;
	}
	public List<MovieShows> getMovieShowsList(){
		return movieshows;
	}
	public void setMovieShowsList(List<MovieShows> movieshows){
		this.movieshows = movieshows;
	}
	
	@Override
	public String toString() {
		return "Theatre [theatreId=" + theatreId + ", theatreName=" + theatreName + ", theatreAddress=" + theatreAddress
				+ "]";
	}

}
