package com.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
@Entity
public class Hoteltickets {

	@Id @GeneratedValue
	   private int ticketId;
	   private LocalDate fromDate;
	   private LocalDate toDate;
	   private int ticketPrice;
	   private int numOfRooms;
	   
	   
	   @ManyToOne
	   Hotel hotel;
	   
	   @ManyToOne
	   Customer customer;
	   
	public Hoteltickets() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Hoteltickets(int ticketId, LocalDate fromDate, LocalDate toDate, int ticketPrice, int numOfRooms) {
		super();
		this.ticketId = ticketId;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.ticketPrice = ticketPrice;
		this.numOfRooms = numOfRooms;
	}
	public int getTicketId() {
		return ticketId;
	}
	public LocalDate getFromDate() {
		return fromDate;
	}
	public LocalDate getToDate() {
		return toDate;
	}
	public int getTicketPrice() {
		return ticketPrice;
	}
	public int getNumOfRooms() {
		return numOfRooms;
	}
	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}
	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}
	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}
	public void setTicketPrice(int ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	public void setNumOfRooms(int numOfRooms) {
		this.numOfRooms = numOfRooms;
	}
	@Override
	public String toString() {
		return "Hoteltickets [ticketId=" + ticketId + ", fromDate=" + fromDate + ", toDate=" + toDate + ", ticketPrice="
				+ ticketPrice + ", numOfRooms=" + numOfRooms + "]";
	}
}
