package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class MetroTicket {
	
	@Id @GeneratedValue
	private int TicketId;
	private int numOfTickets;
	private int totalPrice;
	
	@ManyToOne
	Metro metro;
	
	@ManyToOne
	Customer customer;

	public MetroTicket() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MetroTicket(int ticketId, int numOfTickets, int totalPrice, Metro metro, Customer customer) {
		super();
		TicketId = ticketId;
		this.numOfTickets = numOfTickets;
		this.totalPrice = totalPrice;
		this.metro = metro;
		this.customer = customer;
	}

	public int getTicketId() {
		return TicketId;
	}

	public void setTicketId(int ticketId) {
		TicketId = ticketId;
	}

	public int getNumOfTickets() {
		return numOfTickets;
	}

	public void setNumOfTickets(int numOfTickets) {
		this.numOfTickets = numOfTickets;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Metro getMetro() {
		return metro;
	}

	public void setMetro(Metro metro) {
		this.metro = metro;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "MetroTicket [TicketId=" + TicketId + ", numOfTickets=" + numOfTickets + ", totalPrice=" + totalPrice
				+ ", metro=" + metro + ", customer=" + customer + "]";
	}
	
	

}
