package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class MetroFromTo {
	
	@Id @GeneratedValue
	private int fromtoid;
	private String fromStatons;
	private String toStations;
	public MetroFromTo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MetroFromTo(int fromtoid, String fromStatons, String toStations) {
		super();
		this.fromtoid = fromtoid;
		this.fromStatons = fromStatons;
		this.toStations = toStations;
	}
	public int getFromtoid() {
		return fromtoid;
	}
	public void setFromtoid(int fromtoid) {
		this.fromtoid = fromtoid;
	}
	public String getFromStatons() {
		return fromStatons;
	}
	public void setFromStatons(String fromStatons) {
		this.fromStatons = fromStatons;
	}
	public String getToStations() {
		return toStations;
	}
	public void setToStations(String toStations) {
		this.toStations = toStations;
	}
	@Override
	public String toString() {
		return "MetroFromTo [fromtoid=" + fromtoid + ", fromStatons=" + fromStatons + ", toStations=" + toStations
				+ "]";
	}
	
	
	

}
