package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class MovieShows {

	@Id @GeneratedValue
	private int showId;
	private String showTime;
	private int availableSeats;
	private int ticketPrice;
	
	@ManyToOne
	@JsonIgnore
	private Theatre theatre;
	
	public Theatre getTheatre() {
		return theatre;
	}
	public void setTheatre(Theatre theatre) {
		this.theatre = theatre;
	}
	public MovieShows() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MovieShows(int showId, String showTime, int availableSeats, int ticketPrice) {
		super();
		this.showId = showId;
		this.showTime = showTime;
		this.availableSeats = availableSeats;
		this.ticketPrice = ticketPrice;
	}
	public int getShowId() {
		return showId;
	}
	public void setShowId(int showId) {
		this.showId = showId;
	}
	public String getShowTime() {
		return showTime;
	}
	public void setShowTime(String showTime) {
		this.showTime = showTime;
	}
	public int getAvailableSeats() {
		return availableSeats;
	}
	public void setAvailableSeats(int availableSeats) {
		this.availableSeats = availableSeats;
	}
	public int getTicketPrice() {
		return ticketPrice;
	}
	public void setTicketPrice(int ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	@Override
	public String toString() {
		return "MovieShows [showId=" + showId + ", showTime=" + showTime + ", availableSeats=" + availableSeats
				+ ", ticketPrice=" + ticketPrice + "]";
	}
}
