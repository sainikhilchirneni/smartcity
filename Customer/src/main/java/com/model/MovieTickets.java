package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class MovieTickets {

	@Id @GeneratedValue
	private int ticketId;
	private int noOfTickets;
	private int totalPrice;
	
	@ManyToOne
	private Customer customer;
	
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public MovieTickets() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MovieTickets(int ticketId, int noOfTickets, int totalPrice) {
		super();
		this.ticketId = ticketId;
		this.noOfTickets = noOfTickets;
		this.totalPrice = totalPrice;
	}
	public int getTicketId() {
		return ticketId;
	}
	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}
	public int getNoOfTickets() {
		return noOfTickets;
	}
	public void setNoOfTickets(int noOfTickets) {
		this.noOfTickets = noOfTickets;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	@Override
	public String toString() {
		return "MovieTickets [ticketId=" + ticketId + ", noOfTickets=" + noOfTickets + ", totalPrice=" + totalPrice
				+ "]";
	}
}
