package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Customer 
{
	@Id @GeneratedValue
    int custId;
    String custName;
    String custPhone;
    String custEmail;
    String custPassword;
    
    @OneToMany(mappedBy="customer")
    @JsonIgnore
    List<MetroTicket> metroTicket = new ArrayList<MetroTicket>();
    
    @OneToMany(mappedBy="customer")
    @JsonIgnore
    List<Hoteltickets> hoteltickets = new ArrayList<Hoteltickets>();
    
    @OneToMany(mappedBy="customer")
    @JsonIgnore
    List<MovieTickets> movieTickets = new ArrayList<MovieTickets>();
    
	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Customer(int custId, String custName, String custPhone, String custEmail, String custPassword) {
		super();
		this.custId = custId;
		this.custName = custName;
		this.custPhone = custPhone;
		this.custEmail = custEmail;
		this.custPassword = custPassword;
	}
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustPhone() {
		return custPhone;
	}
	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}
	public String getCustEmail() {
		return custEmail;
	}
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	public String getCustPassword() {
		return custPassword;
	}
	public void setCustPassword(String custPassword) {
		this.custPassword = custPassword;
	}
	public List<MetroTicket> getMetroTicketList(){
		return metroTicket;
	}
	public void setMetroTicket(List<MetroTicket> metroTicket){
		this.metroTicket = metroTicket;
	}
	public List<Hoteltickets> getHotelTicketsList(){
		return hoteltickets;
	}
	public void setHotelTicket(List<Hoteltickets> hoteltickets){
		this.hoteltickets = hoteltickets;
	}
	public List<MovieTickets> getMovieTicketsList(){
		return movieTickets;
	}
	public void setMovieTickets(List<MovieTickets> movieTickets){
		this.movieTickets = movieTickets;
	}
	
	@Override
	public String toString() {
		return "Customer [custId=" + custId + ", custName=" + custName + ", custPhone=" + custPhone + ", custEmail="
				+ custEmail + ", custPassword=" + custPassword + "]";
	}
    
    
}
