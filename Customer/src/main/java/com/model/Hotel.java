package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
public class Hotel {

	@Id @GeneratedValue
	private int hotelId;
	private String hotelName;
	private String address;
	private int roomprice;
	private String image;
	private String description;
	private int availaberooms;
	
	@OneToMany(mappedBy = "hotel")
	@JsonIgnore
	private List<Hoteltickets> hoteltickets=new ArrayList<Hoteltickets>();
	
	public Hotel() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Hotel(int hotelId, String hotelName, String address, int roomprice, String image, String description,
			int availaberooms, List<Hoteltickets> hoteltickets) {
		super();
		this.hotelId = hotelId;
		this.hotelName = hotelName;
		this.address = address;
		this.roomprice = roomprice;
		this.image = image;
		this.description = description;
		this.availaberooms = availaberooms;
		this.hoteltickets = hoteltickets;
	}
	public int getHotelId() {
		return hotelId;
	}
	public String getHotelName() {
		return hotelName;
	}
	public String getAddress() {
		return address;
	}
	public int getRoomprice() {
		return roomprice;
	}
	public String getImage() {
		return image;
	}
	public String getDescription() {
		return description;
	}
	public int getAvailaberooms() {
		return availaberooms;
	}
	public List<Hoteltickets> getHoteltickets() {
		return hoteltickets;
	}
	public void setHotelId(int hotelId) {
		this.hotelId = hotelId;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public void setRoomprice(int roomprice) {
		this.roomprice = roomprice;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setAvailaberooms(int availaberooms) {
		this.availaberooms = availaberooms;
	}
	public void setHoteltickets(List<Hoteltickets> hoteltickets) {
		this.hoteltickets = hoteltickets;
	}
	@Override
	public String toString() {
		return "Hotel [hotelId=" + hotelId + ", hotelName=" + hotelName + ", address=" + address + ", roomprice="
				+ roomprice + ", image=" + image + ", description=" + description + ", availaberooms=" + availaberooms
				+ ", hoteltickets=" + hoteltickets + "]";
	}
}
