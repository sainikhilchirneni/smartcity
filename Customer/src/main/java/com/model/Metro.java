package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Metro {

	@Id @GeneratedValue
	private int metroId=1000;
	private String fromStation;
	private String toStation;
	private int fare;
	private int duration;
	private double distance;
	
	@OneToMany(mappedBy="metro")
	@JsonIgnore
	List<MetroTicket> metroTicket = new ArrayList<MetroTicket>();
	
	
	public Metro() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Metro(int metroId, String fromStation, String toStation, int fare, int duration, double distance) {
		super();
		this.metroId = metroId;
		this.fromStation = fromStation;
		this.toStation = toStation;
		this.fare = fare;
		this.duration = duration;
		this.distance = distance;
	}
	public int getMetroId() {
		return metroId;
	}
	public void setMetroId(int metroId) {
		this.metroId = metroId;
	}
	public String getFromStation() {
		return fromStation;
	}
	public void setFromStation(String fromStation) {
		this.fromStation = fromStation;
	}
	public String getToStation() {
		return toStation;
	}
	public void setToStation(String toStation) {
		this.toStation = toStation;
	}
	public int getFare() {
		return fare;
	}
	public void setFare(int fare) {
		this.fare = fare;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	@Override
	public String toString() {
		return "Metro [metroId=" + metroId + ", fromStation=" + fromStation + ", toStation=" + toStation + ", fare="
				+ fare + ", duration=" + duration + ", distance=" + distance + "]";
	}
	
	
	
}
