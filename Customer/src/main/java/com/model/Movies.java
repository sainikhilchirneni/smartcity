package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Movies {
	
	@Id @GeneratedValue	
	private int movieId;
	private String backgroundImage;
	private String cardImage;
	private String movieTitle;
	private String movieCertificate;
	private String movieRunTime;
	private String movieReleaseDate;
	
	@JsonIgnore
	@OneToMany(mappedBy="movies")
	List<Theatre> theatre = new ArrayList<Theatre>();
	public Movies() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Movies(int movieId, String backgroundImage, String cardImage, String movieTitle, String movieCertificate,
			String movieRunTime, String movieReleaseDate) {
		super();
		this.movieId = movieId;
		this.backgroundImage = backgroundImage;
		this.cardImage = cardImage;
		this.movieTitle = movieTitle;
		this.movieCertificate = movieCertificate;
		this.movieRunTime = movieRunTime;
		this.movieReleaseDate = movieReleaseDate;
	}
	public int getMovieId() {
		return movieId;
	}
	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}
	public String getBackgroundImage() {
		return backgroundImage;
	}
	public void setBackgroundImage(String backgroundImage) {
		this.backgroundImage = backgroundImage;
	}
	public String getCardImage() {
		return cardImage;
	}
	public void setCardImage(String cardImage) {
		this.cardImage = cardImage;
	}
	public String getMovieTitle() {
		return movieTitle;
	}
	public void setMovieTitle(String movieTitle) {
		this.movieTitle = movieTitle;
	}
	public String getMovieCertificate() {
		return movieCertificate;
	}
	public void setMovieCertificate(String movieCertificate) {
		this.movieCertificate = movieCertificate;
	}
	public String getMovieRunTime() {
		return movieRunTime;
	}
	public void setMovieRunTime(String movieRunTime) {
		this.movieRunTime = movieRunTime;
	}
	public String getMovieReleaseDate() {
		return movieReleaseDate;
	}
	public void setMovieReleaseDate(String movieReleaseDate) {
		this.movieReleaseDate = movieReleaseDate;
	}
	@Override
	public String toString() {
		return "Movies [movieId=" + movieId + ", backgroundImage=" + backgroundImage + ", cardImage=" + cardImage
				+ ", movieTitle=" + movieTitle + ", movieCertificate=" + movieCertificate + ", movieRunTime="
				+ movieRunTime + ", movieReleaseDate=" + movieReleaseDate + "]";
	}

}
