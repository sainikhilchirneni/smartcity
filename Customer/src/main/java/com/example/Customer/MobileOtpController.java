package com.example.Customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.NumberParseException;

import com.dao.MobileOtpService;

@RestController
public class MobileOtpController {
	
	@Autowired
    private MobileOtpService smsService;


	@GetMapping("/send-sms")
    public ResponseEntity<String> sendSms(@RequestParam("phoneNumber") String phoneNumber) {
        try {
            PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
            com.google.i18n.phonenumbers.Phonenumber.PhoneNumber parsedPhoneNumber = phoneNumberUtil.parse(phoneNumber, "IN");

            if (phoneNumberUtil.isValidNumber(parsedPhoneNumber)) {
                String formattedPhoneNumber = phoneNumberUtil.format(parsedPhoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
                String message =  "Thank you for registering to Smartcity";
                smsService.sendSms(formattedPhoneNumber, message);
                return ResponseEntity.ok("SMS sent successfully");
            } else {
                return ResponseEntity.badRequest().body("Invalid phone number");
            }
        } catch (NumberParseException e) {
            return ResponseEntity.badRequest().body("Invalid phone number");
        }
    }

}
