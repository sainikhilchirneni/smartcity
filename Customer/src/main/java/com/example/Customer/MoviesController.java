package com.example.Customer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.MoviesDAO;
import com.model.Movies;

@RestController
public class MoviesController {

	@Autowired
	private MoviesDAO moviesDAO;
	
	@PostMapping("registerMovies")
	public Movies registerMovies(@RequestBody Movies movies) {
		return moviesDAO.registerMovies(movies);
	}
	
	@GetMapping("getAllMovies")
	public List<Movies> getAllMovies(){
		return moviesDAO.getAllMovies();
	}
}
