package com.example.Customer;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.MetroTicketDAO;
import com.model.MetroTicket;

@RestController
public class MetroTicketController {
	
	@Autowired
	MetroTicketDAO metroTicketDAO;
	
	
	@PostMapping("registerMetroTicket")
	public MetroTicket registerMetro(@RequestBody MetroTicket metroTicket) {
		return metroTicketDAO.saveTicket(metroTicket);
	}
	
	

}
