package com.example.Customer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.MovieShowsDAO;
import com.model.MovieShows;

@RestController
public class MovieShowsController {

	@Autowired
	private MovieShowsDAO movieshowsDAO;
	
	@PostMapping("registerMovieShows")
	public MovieShows registerMovieShows(@RequestBody MovieShows movieshows) {
		return movieshowsDAO.registerMovieShows(movieshows);
	}
	
	@GetMapping("getmovieshows/{theatreId}/{showTime}")
	public MovieShows getShowsByTheatreIdAndShowTime(@PathVariable int theatreId, @PathVariable String showTime) {
        return movieshowsDAO.getShows(theatreId, showTime);
    }
	@PostMapping("updateAvailableSeats/{showId}/{newAvailableSeats}")
    public MovieShows updateAvailableSeats(
            @PathVariable int showId,
            @PathVariable int newAvailableSeats) {
        return movieshowsDAO.updateAvailableSeats(showId, newAvailableSeats);
    }
}
