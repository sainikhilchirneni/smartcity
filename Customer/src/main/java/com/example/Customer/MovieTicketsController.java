package com.example.Customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.MovieTicketsDAO;
import com.model.MovieTickets;

@RestController
public class MovieTicketsController {

	@Autowired
	private MovieTicketsDAO movieticketsDAO;
	
	@PostMapping("registerMovieTickets")
	public MovieTickets registerMovieTickets(@RequestBody MovieTickets movietickets) {
		return movieticketsDAO.registerMovieTickets(movietickets);
	}
}
