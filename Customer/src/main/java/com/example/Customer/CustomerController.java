package com.example.Customer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CustomerDAO;
import com.model.Customer;

@RestController
public class CustomerController 
{
	@Autowired
    private CustomerDAO custDAO;
	//@Autowired
	//private PasswordEncoder passwordEncoder;
    
    @GetMapping("custLogin/{emailId}/{password}")
	public Customer custLogin(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
    	//password = passwordEncoder.encode(password);
		return custDAO.custLogin(emailId, password);
	}
    
    @PostMapping("registerCustomer")
	public Customer registerCust(@RequestBody Customer cust) {
    	//cust.setCustPassword(passwordEncoder.encode(cust.getCustPassword()));
		return custDAO.registerCust(cust);
	}
    @GetMapping("getAllCustomers")
	public List<Customer> getAllCustomers() {
		return custDAO.getAllCustomers();
	}

}
