package com.example.Customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.dao.QrCodeService;

@RestController
@RequestMapping("/api")
public class QrCodeController {
	
	@Autowired
    private QrCodeService qrCodeService;

    @PostMapping("/generateQRCode")
    public ResponseEntity<byte[]> generateQRCode(@RequestBody String details) {
        try {
            byte[] qrCodeImage = qrCodeService.generateQRCodeImage(details, 300, 300	);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.IMAGE_PNG);
            return new ResponseEntity<>(qrCodeImage, headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
