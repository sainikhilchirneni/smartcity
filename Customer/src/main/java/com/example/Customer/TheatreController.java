package com.example.Customer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.TheatreDAO;
import com.model.Theatre;

@RestController
public class TheatreController {

	@Autowired
	private TheatreDAO theatreDAO;
	
	@PostMapping("registerTheatre")
	public Theatre registerTheatre(@RequestBody Theatre theatre) {
		return theatreDAO.registerTheatre(theatre);
	}
	
	@GetMapping("/byMovieId/{movieId}")
    public List<Theatre> getTheatresByMovieId(@PathVariable int movieId) {
        return theatreDAO.getTheatres(movieId);
    }
}
