package com.example.Customer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.HotelticketDAO;
import com.model.Hoteltickets;

@RestController
public class hotelticketController {

	@Autowired
	HotelticketDAO hotelticketDAO;
     
	@PostMapping("registerHotelTicket")
	public Hoteltickets Register(@RequestBody Hoteltickets hotelTicket)
	{
		return hotelticketDAO.RegisterHotelticket(hotelTicket);
	}
   @GetMapping("GetAllHotelTickets")
   public List<Hoteltickets> getAllHotelTickets()
   {
	   return hotelticketDAO.getAllHoteltickets();
   }
   
   @PutMapping("updateHotelTicket")
	public Hoteltickets update(@RequestBody Hoteltickets hotelTicket)
	{
		return hotelticketDAO.UpdateHotelticket(hotelTicket);
	}
}
