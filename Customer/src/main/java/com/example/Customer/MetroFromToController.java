package com.example.Customer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.MetroFromToDAO;
import com.model.MetroFromTo;

@RestController
public class MetroFromToController
{
	@Autowired
	MetroFromToDAO metrofromtodao;
	@GetMapping("GetAllMetrofromto")
    public List<MetroFromTo> getAllmetrofromto()
    {
		return metrofromtodao.getAllMetroFromto();
    }
    @PostMapping("RegisterAsllmetrofromto")
	public MetroFromTo register(MetroFromTo metrofromto)
	{
		return metrofromtodao.RegisterMetroFromto(metrofromto);
	}
}
