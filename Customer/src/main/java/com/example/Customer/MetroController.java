package com.example.Customer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.MetroDAO;
import com.model.Metro;

@RestController
public class MetroController {
	
	@Autowired
	MetroDAO metroDAO;
	
	@PostMapping("registerMetro")
	public Metro registerMetro(@RequestBody Metro metro) {
		return metroDAO.metroRegister(metro);
	}
	
	@GetMapping("getAllMetro")
	public List<Metro> getAllMetro() {
		return metroDAO.getAllMetro();
	}
	
	 @GetMapping("getMetrodetails/{fromStation}/{toStation}")
		public Metro getMetrodetails(@PathVariable("fromStation") String fromStation, @PathVariable("toStation") String toStation) {
	    
			return metroDAO.getMetroDetails(fromStation, toStation);
		}

}
