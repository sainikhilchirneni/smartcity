package com.example.Customer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.HotelDAO;
import com.model.Hotel;

@RestController
public class HotelController {

	@Autowired
    HotelDAO hotelDAO;
	@PostMapping("registerHotel")
	public Hotel Register(@RequestBody Hotel hotel)
	{
		System.out.println(hotel);
		return hotelDAO.RegisterHotel(hotel);
	}
   @GetMapping("GetAllHotels")
   public List<Hotel> getAllhotels()
   {
	   return hotelDAO.getAllHotels();
   }
   
   @PutMapping("updateHotel")
	public Hotel update(@RequestBody Hotel hotel)
	{
		return hotelDAO.UpdateHotel(hotel);
	}
}
